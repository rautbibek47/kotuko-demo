<?php

use App\Events\CustomerEvent;
use App\Http\Controllers\NewsController;
use App\Mail\SendEmailTest;
use App\Models\Customer;
use App\Models\Product;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{section}',[NewsController::class,'index'])->name('news');
Route::get('/', function () {
    return view('welcome');
});
