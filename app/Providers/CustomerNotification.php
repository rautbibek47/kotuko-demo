<?php

namespace App\Providers;

use App\Events\CustomerEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CustomerNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CustomerEvent  $event
     * @return void
     */
    public function handle(CustomerEvent $event)
    {
        //
    }
}
